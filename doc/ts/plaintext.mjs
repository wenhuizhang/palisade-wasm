import {doc} from './doc.mjs'

doc('GetLength', [],`
/**
* Get method to return the length of plaintext
*
* @return the length of the plaintext in terms of the number of bits.
*/ `)

doc('SetLength', ['newSize'], `
/**
* resize the plaintext; only works for plaintexts that support a resizable
* vector (coefpacked)
* @param newSize
*/ `)

doc('GetLogPrecision', [], `
/**
* Get method to return log2 of estimated precision
*/ `)

doc('Plaintext', [], `
/**
 * A plaintext describes a value that has been prepared for the cryptocontext,
 * but is not encrypted
 */ `)
