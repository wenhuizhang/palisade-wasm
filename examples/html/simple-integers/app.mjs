import factory from '../lib/palisade_pke_es6.js'

const dom = {
	plaintextModulus: document.getElementById('plaintext-modulus'),
	sigma: document.getElementById('sigma'),
	depth: document.getElementById('depth'),
	securityLevel: document.getElementById('security-level'),
	plaintext1: document.getElementById('plaintext1'),
	plaintext2: document.getElementById('plaintext2'),
	plaintext3: document.getElementById('plaintext3'),
	log: document.getElementById('log'),
	applyButton: document.getElementById('apply-button'),
};

let lastLog = null;

function log(msg) {
	const node = document.createElement('div');
	const left = document.createElement('span');
	const right = document.createElement('span');
	left.innerText = msg
	const now = performance.now();
	right.innerText = `${(now-lastLog).toFixed(0)} ms`;
	node.appendChild(left);
	node.appendChild(right);
	dom.log.appendChild(node);
}

function logerror(msg) {
	const node = document.createElement('div');
	node.className = "error";
	node.innerText = msg;
	dom.log.appendChild(node);
}

// use wrapper async function to re-use module
let module = null;
async function getModule() {
	if (!module) {
		console.log('initializing module');
		module = await factory();
	}
	return module;
}

const parseCommaArray = array =>
	array.split(',').map(x=>Number.parseInt(x));

async function runDemo() {
	dom.log.innerText="";
	lastLog = performance.now();
	log("Starting demo");
	const module = await getModule();
	log("Initialized module");
	const handles = [];
	let cryptoContext = null;
	try {
		const plaintextModulus = Number.parseInt(dom.plaintextModulus.value);
		const sigma = Number.parseFloat(dom.sigma.value);
		const depth = Number.parseInt(dom.depth.value);
		const securityLevel = module.SecurityLevel[dom.securityLevel.value];
		// store WASM object references for deletion later
		cryptoContext = new module.GenCryptoContextBFVrns(
			plaintextModulus, securityLevel,
			sigma, 0, depth, 0, module.MODE.OPTIMIZED
		);
		handles.push(cryptoContext);
		log("Generated crypto context");
		cryptoContext.Enable(module.PKESchemeFeature.ENCRYPTION);
		cryptoContext.Enable(module.PKESchemeFeature.SHE);
		// Initialize Public Key Containers
		// Define LPKeyPair<DCRTPoly> type then replace "any" type below
		// Generate a public/private key pair
		const keyPair = cryptoContext.KeyGen();
		handles.push(keyPair);
		// need to be VERY careful about lifetime semantics.
		// Even accessing this property causes a shared pointer copy
		// which can leave objects alive forever
		const secretKey = keyPair.secretKey;
		handles.push(secretKey);
		// Generate the relinearization key
		cryptoContext.EvalMultKeyGen(secretKey);
		// NOTE that we do not use this key but since it was returned to JS,
		// we MUST delete it manually
		cryptoContext.EvalAtIndexKeyGen(secretKey, [1,2,-1,-2]);
		// Sample Program: Step 3: Encryption
		// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Typed_arrays
		// First plaintext vector is encoded (64bit signed in C/C++ => BigInt64Array
		// in JS)
		const vectorOfInts1 = module.MakeVectorInt64Clipped(
			parseCommaArray(dom.plaintext1.value));
		handles.push(vectorOfInts1);
		//"Plaintext" type is switched to string
		const plaintext1 = cryptoContext.MakePackedPlaintext(vectorOfInts1);
		handles.push(plaintext1);
		// Second plaintext vector is encoded (64bit signed in C/C++ => BigInt64Array
		// in JS)
		const vectorOfInts2 = module.MakeVectorInt64Clipped(
			parseCommaArray(dom.plaintext2.value));
		handles.push(vectorOfInts2);
		//"Plaintext" type is switched to string
		const plaintext2 = cryptoContext.MakePackedPlaintext(vectorOfInts2);
		handles.push(plaintext2);
		// Third plaintext vector is encoded (64bit signed in C/C++ => BigInt64Array
		// in JS)
		const vectorOfInts3 = module.MakeVectorInt64Clipped(
			parseCommaArray(dom.plaintext3.value))
		handles.push(vectorOfInts3);
		//"Plaintext" type is switched to string
		const plaintext3 = cryptoContext.MakePackedPlaintext(vectorOfInts3);
		handles.push(plaintext3);
		// The encoded vectors are encrypted
		const ciphertext1 = cryptoContext.Encrypt(keyPair.publicKey, plaintext1);
		handles.push(ciphertext1);
		const ciphertext2 = cryptoContext.Encrypt(keyPair.publicKey, plaintext2);
		handles.push(ciphertext2);
		const ciphertext3 = cryptoContext.Encrypt(keyPair.publicKey, plaintext3);
		handles.push(ciphertext3);
		// Sample Program: Step 4: Evaluation
		// all "auto" types become "any"
		// Homomorphic additions
		const ciphertextAdd12 = cryptoContext.EvalAddCipherCipher(ciphertext1, ciphertext2);
		handles.push(ciphertextAdd12);
		const ciphertextAddResult = cryptoContext.EvalAddCipherCipher(ciphertextAdd12, ciphertext3);
		handles.push(ciphertextAddResult);
		// Homomorphic multiplications
		const ciphertextMul12 = cryptoContext.EvalMultCipherCipher(ciphertext1, ciphertext2);
		handles.push(ciphertextMul12);
		const ciphertextMultResult = cryptoContext.EvalMultCipherCipher(ciphertextMul12, ciphertext3);
		handles.push(ciphertextMultResult);
		// Homomorphic rotations
		const ciphertextRot1 = cryptoContext.EvalAtIndex(ciphertext1, 1);
		handles.push(ciphertextRot1);
		const ciphertextRot2 = cryptoContext.EvalAtIndex(ciphertext1, 2);
		handles.push(ciphertextRot2);
		const ciphertextRot3 = cryptoContext.EvalAtIndex(ciphertext1, -1);
		handles.push(ciphertextRot3);
		const ciphertextRot4 = cryptoContext.EvalAtIndex(ciphertext1, -2);
		handles.push(ciphertextRot4);
		// Sample Program: Step 5: Decryption
		// Decrypt the result of additions
		// Plaintext => any
		let plaintextAddResult = cryptoContext.Decrypt(secretKey, ciphertextAddResult);
		handles.push(plaintextAddResult);
		// Decrypt the result of multiplications
		// Plaintext => any
		let plaintextMultResult = cryptoContext.Decrypt(secretKey, ciphertextMultResult);
		handles.push(plaintextMultResult);
		// Decrypt the result of rotations
		//"Plaintext" => "implicit, emscripten will handle"
		let plaintextRot1 = cryptoContext.Decrypt(secretKey, ciphertextRot1);
		handles.push(plaintextRot1);
		let plaintextRot2 = cryptoContext.Decrypt(secretKey, ciphertextRot2);
		handles.push(plaintextRot2);
		let plaintextRot3 = cryptoContext.Decrypt(secretKey, ciphertextRot3);
		handles.push(plaintextRot3);
		let plaintextRot4 = cryptoContext.Decrypt(secretKey, ciphertextRot4);
		handles.push(plaintextRot4);

		plaintextRot1.SetLength(vectorOfInts1.size());
		plaintextRot2.SetLength(vectorOfInts1.size());
		plaintextRot3.SetLength(vectorOfInts1.size());
		plaintextRot4.SetLength(vectorOfInts1.size());

		// Output results
		log('\nResults of homomorphic computations');
		log(`#1 + #2 + #3: ${plaintextAddResult}`);
		log(`#1 * #2 * #3: ${plaintextMultResult}`);
		log(`Left rotation of #1 by 1: ${plaintextRot1}`);
		log(`Left rotation of #1 by 2: ${plaintextRot2}`);
		log(`Right rotation of #1 by 1: ${plaintextRot3}`);
		log(`Right rotation of #1 by 2: ${plaintextRot4}`);
	}
	catch (error) {
		const message = typeof error === "number" ?
			module.getExceptionMessage(error) : error;
		logerror(message);
	}
	finally {
		if (cryptoContext) {
			// crypto context populates some cache entries
			// which need to be deleted
			cryptoContext.ClearEvalMultKeys();
			cryptoContext.ClearEvalAutomorphismKeys();
		}
		for (const handle of handles) {
			handle.delete();
		}
	}
}

dom.applyButton.addEventListener('click',runDemo);
